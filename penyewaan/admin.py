from django.contrib import admin
# from .models import Kamar
# from .models import Penyewa
from .models import Sewa

# Register your models here.
@admin.register(Sewa)
class SewaAdmin(admin.ModelAdmin):
    list_display = ['nama_penyewa','nama_kamar','harga','tgl_input','user']
    list_filter = ['nama_penyewa','nama_kamar','harga','tgl_input']
    search_fields = ['nama_penyewa','nama_kamar','tgl_input']