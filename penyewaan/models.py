from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
# Create your models here.

class Sewa(models.Model):
    KAMAR_CHOICES = (
        ('AI','Standart Room'),
        ('A2','Superior Room'),
    )
    nama_penyewa = models.CharField('Nama Lengkap', max_length=100, null=False)
    nama_kamar = models.CharField(max_length=2, choices=KAMAR_CHOICES)
    harga = models.FloatField('Harga (Rp)')
    tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl_input']

    def __str__(self):
        return self.nama_penyewa

    def get_absolute_url(self):
        return reverse('sewa')