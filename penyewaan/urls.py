from django.urls import path
from .views import index, sewa, SewaDetailView, SewaCreateView
from .views import SewaEditView, SewaDeleteView, SewaToPdf

urlpatterns = [
    path('', index, name='home_page'),
    path('sewa', sewa, name='sewa'),
    path('sewa/<int:pk>', SewaDetailView.as_view(),
        name='sewa_detail_view'),
    path('sewa/add', SewaCreateView.as_view(), name='sewa_add'),
    path('sewa/edit/<int:pk>', SewaEditView.as_view(), name='sewa_edit'),
    path('sewa/delete/<int:pk>', SewaDeleteView.as_view(), name='sewa_delete'),
    path('sewa/print_pdf', SewaToPdf.as_view(), name='sewa_to_pdf')
]